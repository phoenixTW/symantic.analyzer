var _ = require('lodash');

var generateSubsetWith = function (keys, collection) {
    return keys.reduce(function (subset, key) {
      subset[key] = collection[key];
      return subset;
    }, {});
};

var Collection = function (collection) {
  this.collection = collection;
}

Collection.prototype = {
  trim: function () {
    return new Collection(this.collection.filter(function(element) {
      return !_.isEmpty(element);
    }));
  },

  filter: function (predicate) {
    return new Collection(this.collection.filter(function(element) {
        var subset = generateSubsetWith(Object.keys(predicate), element);
        return _.isEqual(subset, predicate);
    }));
  },

  findAll: function (key) {
    return new Collection(this.collection.map(function(element){
      return element[key];
    }));
  },

  union: function (key, sentences) {
    return new Collection(_.union(this.collection));
  },

  isEmpty: function () {
    return _.isEmpty(this.collection);
  },

  join: function (string) {
    return this.collection.join(string);
  },

  get: function() {
    return this.collection;
  }
};

module.exports = Collection;
