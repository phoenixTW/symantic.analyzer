Grammar Semantic Analyser
=========================

Description
-----------
Grammar Semantic Analyser *English* language sentences but we can do the same thing for other languages as well. It uses javascript parser Jison[http://zaa.ch/jison/] for parsing English sentences.
