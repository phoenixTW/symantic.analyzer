var _ = require('lodash');
var parser = require('./symantic');
var Collection = require('./utils/collection');

var getAllSentencesFor = function (finder, tokens) {
  var key = Object.keys(finder)[0];
  var extractSentencesForNounFrom = function (token) {
    return token[key] == finder[key];
  };
  return tokens.filter(extractSentencesForNounFrom);
}

var getAllSentencesBy = function (verbs, tokens) {
  var nouns = tokens.findAll("NOUN").union().get();
  return nouns.map(function (noun) {
    var tokensBasedOnNoun = getAllSentencesFor({ 'NOUN': noun }, tokens.get());

    return _.flattenDeep(verbs.map(function(verb) {
      return getAllSentencesFor({ 'VERB': verb }, tokensBasedOnNoun);
    }));
  });
};

var generateConjunctiveObjects = function (sentences) {
  var products = sentences.map(function (sentence) {
    return sentence.OBJECT;
  });

  if(products.length == 1) return products.join("");
  if(products.length == 2) return products.join(" and ");
  return products.slice(0, products.length - 1).join(", ") + " and " + _.last(products);
};

var createConjunctiveSentencesFor = function (collectionOfSentences) {
  var nouns = collectionOfSentences.findAll("NOUN").union().get();
  return nouns.map(function (noun) {
    var verbs = collectionOfSentences.findAll("VERB").union().get();
    var sentence = verbs.map(function (verb) {
      var sentences = collectionOfSentences.filter({'NOUN': noun, 'VERB': verb}).trim();
      if(sentences.isEmpty()) return;
      return noun + " " + verb + " " +
       generateConjunctiveObjects(sentences.get()) + sentences.findAll("DOT").union().get();
    });
    return new Collection(sentence).trim().join("\n");
  }).join("\n");
};

var Compiler = function (sentences) {
  this.sentences = sentences;
};

Compiler.prototype.compile = function () {
  var tokens = parser.parse(this.sentences);
  var verbs = new Collection(tokens).findAll("VERB").union().get();
  var allSentencesByVerbs = _.flattenDeep(getAllSentencesBy(verbs, new Collection(tokens)));
  return createConjunctiveSentencesFor(new Collection(allSentencesByVerbs));
};

module.exports = Compiler;
