%lex

%%

\s+                               /* Skip white spaces */
"ram"                             { return 'TOKEN_NOUN'; }
"sita"                            { return 'TOKEN_NOUN'; }

"also"                            { return 'TOKEN_ADVERB'; }

"tea"                             { return 'TOKEN_OBJECT'; }
"coffee"                          { return 'TOKEN_OBJECT'; }
"butter"                          { return 'TOKEN_OBJECT'; }
"cheese"                          { return 'TOKEN_OBJECT'; }
"biscuits"                        { return 'TOKEN_OBJECT'; }

"hates"                           { return 'TOKEN_VERB'; }
"likes"                           { return 'TOKEN_VERB'; }

"."                               { return 'TOKEN_DOT'; }

<<EOF>>                           { return 'TOKEN_EOF'; }

/lex

%start PARAGRAPH

%%

PARAGRAPH
  : SENTENCES EOF
  {{
    return $$;
  }}
  ;

SENTENCES
  : SENTENCE
  {{
    $$ = [$1];
  }}
  | SENTENCES SENTENCE
  {{
    $$ = $1.concat($2);
  }}
  ;

SENTENCE
  : NOUN VERB OBJECT DOT
  {
      $$ = { 'NOUN': $1, 'VERB': $2, 'OBJECT': $3, 'DOT': $4 }
  }
  | NOUN ADVERB VERB OBJECT DOT
  {
      $$ = { 'NOUN': $1, 'ADVERB': $2, 'VERB': $3, 'OBJECT': $4, 'DOT': $5 }
  }
  ;

NOUN
  : TOKEN_NOUN
  {{
      $$ = $1
  }}
  ;

ADVERB
  : TOKEN_ADVERB
  {{
      $$ = $1
  }}
  ;

OBJECT
  : TOKEN_OBJECT
  {{
      $$ = $1
  }}
  | TOKEN_NOUN
  {{
    $$ = $1
  }}
  ;

VERB
  : TOKEN_VERB
  {{
      $$ = $1
  }}
  ;

DOT
  : TOKEN_DOT
  {{
      $$ = $1
  }}
  ;

EOF
  : TOKEN_EOF
  {{
      $$ = $1
  }}
  ;
