var chai = require('chai');
var assert = chai.assert;

var Compiler = require('../compiler');

describe("compiler", function () {
  describe("#compile", function() {
    it("should perform conjunction for likes present in two sentences", function () {
      var sentences = "ram likes tea.\nram likes coffee.";
      var expected = "ram likes tea and coffee."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });

    it("should perform conjunction for hates present in two sentences", function () {
      var sentences = "ram hates butter.\nram hates cheese.";
      var expected = "ram hates butter and cheese."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });

    it("should perform conjunction for both two likes sentences and two hates sentences", function () {
      var sentences = "ram likes tea.\nram likes coffee.\nram hates butter.\nram hates cheese.";
      var expected = "ram likes tea and coffee.\nram hates butter and cheese."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });

    it("should perform conjunction 3 likes sentences with different products", function () {
      var sentences = "ram likes tea.\nram likes coffee.\nram likes biscuits.\nram hates butter.\nram hates cheese.";
      var expected = "ram likes tea, coffee and biscuits.\nram hates butter and cheese."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });

    it("should perform conjunction two different nouns", function () {
      var sentences = "ram likes tea.\nsita likes ram.\nram likes coffee.\nram hates butter.\nram hates cheese.";
      var expected = "ram likes tea and coffee.\nram hates butter and cheese.\nsita likes ram."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });

    it("should perform conjunction with an also sentence", function() {
      var sentences = "ram likes tea.\nram also likes coffee.\nram hates butter.\nram also hates cheese.";
      var expected = "ram likes tea and coffee.\nram hates butter and cheese."
      var compiler = new Compiler(sentences);
      assert.deepEqual(compiler.compile(), expected);
    });
  });
});
